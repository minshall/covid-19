# we are decommissioned.  point everything at ourworldindata.

all: release

GNUMAKEFLAGS += --warn-undefined-variables

DDIR = ./public
DDIRS = ${DDIR}

HTMLS = ${DDIR}/index.html ${DDIR}/world.html

RSYNC = rsync
RSYNCOPTS = -az --info=progress2 --delete

# releasenodep has *no* dependencies -- it assumes everything is built and
# sitting in DDIR, ready to go to RDIR.  notice the slash ('/') after
# ${DDIR} -- that's magic to tell rsync that the contents of ${DDIR},
# but not ${DDIR} itself, are to be copied.

# release directory: where files go when we are ready to release; you
# can symbolically link anywhere you want in your file system, or
# override on the command line
RDIR = ./release

# insure we have all the directories needed to build
dirs:
	@for dir in ${DDIRS}; do \
		if [ ! -e $${dir} ]; then mkdir $${dir}; fi \
	done


# copy whatever files have previously been built into the release
# directory (typically on somenumbers.info/covid-19)
releasenodep:
	${RSYNC} ${RSYNCOPTS} ${DDIR}/ ${RDIR}

.PHONY : release

# build and release the web site
release: ${HTMLS} releasenodep

${DDIR}/index.html: index.html | dirs
	cp -p $< $@

${DDIR}/world.html: index.html | dirs
	cp -p $< $@

index.html: index.org
	emacs $< --batch -f org-html-export-as-html --kill

