the following table is a sedcsv script file.

note that this doesn't fix everything.  there is a file "fixups.sed"
that runs before sedcsv (see Makefile), and then the code itself fixes
things.  for example
: "Chatham County, NC",United States,2020-03-06T16:13:15,1,0,0,35.7211,-79.1781
exists in 03-06-2020.csv, even after sed and sedcsv.  it is cleaned up
in =fipxit()= in the code (by, specifically, a call to =fipsfa2ps()=).

#+name: sedcsvtable
| FIPS              | Admin2                                  | Country_Region                                                        | Province_State                                                      |
|                   |                                         |                                                                       | /^None$/;c                                                          |
|                   |                                         | /^Mainland China$/;c China                                            |                                                                     |
|                   |                                         | /^US$/;c United States                                                |                                                                     |
|                   |                                         | /^UK$/;c United Kingdom                                               |                                                                     |
|                   |                                         | /^United States$/                                                     | /^US$/;c Unknown                                                    |
|                   |                                         | /^Denmark$/                                                           | /^$/;c Unknown                                                      |
|                   |                                         | /^Denmark$/                                                           | /^Denmark$/;c Unknown                                               |
|                   |                                         | /^France$/                                                            | /^$/;c Unknown                                                      |
|                   |                                         | /^France$/                                                            | /^France$/;c Unknown                                                |
| /^00250$/;c       |                                         | /^France$/                                                            |                                                                     |
| /^250$/;c         |                                         | /^France$/                                                            |                                                                     |
|                   |                                         | /^France$/                                                            | /^Fench Guiana$/;c French Guiana                                    |
|                   |                                         | /^United Kingdom$/                                                    | /^$/;c Unknown                                                      |
|                   |                                         | /^UK$/                                                                | /^$/;c Unknown                                                      |
|                   |                                         | /^United Kingdom$/                                                    | /^UK$/;c Unknown                                                    |
|                   |                                         | /^United Kingdom$/                                                    | /^United Kingdom$/;c Unknown                                        |
|                   |                                         | /^Netherlands$/                                                       | /^$/;c Unknown                                                      |
|                   |                                         | /^New Zealand$/                                                       | /^$/;c Unknown                                                      |
|                   |                                         | /^Netherlands$/                                                       | /^Netherlands$/;c Unknown                                           |
|                   |                                         | /^New Zealand$/                                                       | /^$/;c Unknown                                                      |
|                   |                                         | /^The Bahamas$/;c Bahamas                                             |                                                                     |
|                   |                                         | /^Bahamas, The$/;c Bahamas                                            |                                                                     |
|                   |                                         | /^Aruba$/;c Netherlands                                               | c Aruba                                                             |
|                   |                                         | /^Curacao$/;c Netherlands                                             | c Curacao                                                           |
|                   |                                         | /^Republic of Moldova$/;c Moldova                                     |                                                                     |
|                   |                                         | /^Viet Nam$/;c Vietnam                                                |                                                                     |
|                   |                                         | /^Russia$/;c Russian Federation                                       |                                                                     |
|                   |                                         | /^Taipei and environs$/;c Taiwan, Province of China                   | /^Taiwan$/;c                                                        |
|                   |                                         | /^Taiwan$/;c Taiwan, Province of China                                | /^Taiwan$/;c                                                        |
|                   |                                         | /^Taiwan$/;c Taiwan, Province of China                                |                                                                     |
|                   |                                         | /^Republic of Korea$/;c Korea, Rep.                                   |                                                                     |
|                   |                                         | /^Korea, South$/;c Korea, Rep.                                        |                                                                     |
|                   |                                         | /^South Korea$/;c Korea, Rep.                                         |                                                                     |
|                   |                                         | /^East Timor$/;c Timor-Leste                                          |                                                                     |
|                   |                                         | /^Czechia$/;c Czech Republic                                          |                                                                     |
|                   |                                         | /^Canada$/                                                            | /^Calgary, Alberta$/;c Alberta                                      |
|                   |                                         | /^Canada$/                                                            | /^Edmonton, Alberta$/;c Alberta                                     |
|                   |                                         | /^Canada$/                                                            | /^London, ON$/;c Ontario                                            |
|                   |                                         | /^Canada$/                                                            | /^Toronto, ON$/;c Ontario                                           |
|                   |                                         | /^Canada$/                                                            | /^Montreal, QC$/;c Quebec                                           |
|                   |                                         | /^India$/                                                             | /^Dadar Nagar Haveli$/;c Dadra and Nagar Haveli and Daman and Diu   |
|                   |                                         | /^Iran (Islamic Republic of)$/;c Iran, Islamic Rep.                   |                                                                     |
|                   |                                         | /^Iran$/;c Iran, Islamic Rep.                                         |                                                                     |
|                   |                                         | /^Hong Kong$/;c China                                                 | /^Hong Kong$/;c Hong Kong SAR                                       |
|                   |                                         | /^Hong Kong SAR$/;c China                                             | /^Hong Kong$/;c Hong Kong SAR                                       |
|                   |                                         | /^China$/;c China                                                     | /^Hong Kong$/;c Hong Kong SAR                                       |
|                   |                                         | /^Macao SAR$/;c China                                                 | c Macau SAR                                                         |
|                   |                                         | /^Macau$/;c China                                                     | /^Macau$/;c Macau SAR                                               |
|                   |                                         | /^China$/                                                             | /^Macau$/;c Macau SAR                                               |
|                   |                                         | /^Laos$/;c Lao PDR                                                    |                                                                     |
|                   |                                         | /^Burma$/;c Myanmar                                                   |                                                                     |
|                   |                                         | /^Germany$/                                                           | /^Bavaria$/;c Bayern                                                |
|                   |                                         | /^occupied Palestinian territory$/;c Palestine, State of              |                                                                     |
|                   |                                         | /^Palestine$/;c Palestine, State of                                   |                                                                     |
| c 66000           |                                         | /^Guam$/;c United States                                              | c Guam                                                              |
|                   |                                         | /^Puerto Rico$/;c United States                                       | c Puerto Rico                                                       |
|                   |                                         | /^United States$/                                                     | /^Virgin Islands$/;c U.S. Virgin Islands                            |
| c 69000           |                                         | /^United States$/                                                     | /^Northern Mariana Islands$/                                        |
|                   |                                         |                                                                       | /^Virgin Islands, U.S.$/;c U.S. Virgin Islands                      |
|                   |                                         |                                                                       | /^United States Virgin Islands$/;c U.S. Virgin Islands              |
| c 78000           |                                         |                                                                       | /^U.S. Virgin Islands$/                                             |
|                   | /^LaSalle$/;c La Salle                  |                                                                       | /^Louisiana$/                                                       |
|                   | /^Desoto$/;c DeSoto                     |                                                                       | /^Florida$/                                                         |
|                   | c Alameda County                        |                                                                       | /^Berkeley, CA$/;c California                                       |
|                   | c Saunders County                       |                                                                       | /^Ashland, NE$/;c NE                                                |
|                   | c Suffolk County                        |                                                                       | /^Boston, MA$/;c MA                                                 |
|                   | c Norfolk County                        |                                                                       | /^ Norfolk County, MA/;c MA                                         |
|                   | /^Brockton$/;c Plymouth                 |                                                                       | /^Massachusetts$/                                                   |
|                   | c Chicago                               |                                                                       | /^Chicago$/;c IL                                                    |
|                   | c Chicago                               |                                                                       | /^Chicago, IL$/;c IL                                                |
|                   | c Hillsborough County                   |                                                                       | /^Hillsborough, FL$/;c FL                                           |
|                   | /^Portland$/;c Multnomah                |                                                                       | /^OR$/                                                              |
|                   | c New York City                         |                                                                       | /^New York, NY$/;c NY                                               |
|                   | c New York City                         |                                                                       | /^New York City, NY$/;c NY                                          |
|                   | /^LeSeur$/;c Sibley                     |                                                                       | /^Minnesota$/                                                       |
|                   | c Bexar                                 |                                                                       | /^San Antonio, TX$/;c TX                                            |
|                   | c Portland                              |                                                                       | /^Portland, OR$/;c OR                                               |
|                   | c Unassigned                            |                                                                       | /^Unassigned Location, WA$/;c WA                                    |
|                   | c Unassigned                            |                                                                       | /^Unassigned Location, VT$/;c VT                                    |
|                   | c Unassigned                            |                                                                       | /^Unknown Location, MA$/;c MA                                       |
|                   | /^Dona Ana$/;c Doña Ana                 |                                                                       | /^New Mexico$/                                                      |
|                   | /^Unknown$/;c Unassigned                |                                                                       | /^Tennessee$/                                                       |
|                   | /^unassigned$/;c Unassigned             |                                                                       | /^Utah$/                                                            |
|                   | /^unassigned$/;c Unassigned             |                                                                       | /^Wyoming$/                                                         |
|                   | c Bexar County                          |                                                                       | /^Lackland, TX$/;c TX                                               |
|                   | c Bexar County                          |                                                                       | /^Lackland, TX (From Diamond Princess)$/;c TX                       |
|                   | c Los Angeles County                    |                                                                       | /^Los Angeles, CA$/;c CA                                            |
|                   | c Dane County                           |                                                                       | /^Madison, WI$/;c WI                                                |
|                   | c Plymouth County                       |                                                                       | /^Norwell County, MA$/;c MA                                         |
|                   | c Douglas County                        |                                                                       | /^Omaha, NE (From Diamond Princess)$/;c NE                          |
|                   | c Orange County                         |                                                                       | /^Orange, CA$/;c CA                                                 |
|                   | c Providence County                     |                                                                       | /^Providence, RI$/;c RI                                             |
|                   | c San Benito County                     |                                                                       | /^San Benito, CA$/;c CA                                             |
|                   | c San Mateo County                      |                                                                       | /^San Mateo, CA$/;c CA                                              |
|                   | c Santa Clara County                    |                                                                       | /^Santa Clara, CA$/;c CA                                            |
|                   | c Sarasota County                       |                                                                       | /^Sarasota, FL$/;c FL                                               |
|                   | c King County                           |                                                                       | /^Seattle, WA$/;c WA                                                |
|                   | c Maricopa County                       |                                                                       | /^Tempe, AZ$/;c AZ                                                  |
|                   | c Solano County                         |                                                                       | /^Travis, CA$/;c CA                                                 |
|                   | c Solano County                         |                                                                       | /^Travis, CA (From Diamond Princess)$/;c CA                         |
| /^32007$/         | /^Elko County$/;c Elko                  |                                                                       |                                                                     |
| /^53071$/         | /^Walla Walla County$/;c Walla Walla    |                                                                       |                                                                     |
| /^90049$/         | /^Southwest$/;c Southwest Utah          |                                                                       | /^Utah$/                                                            |
| /^$/;c 90049      | /^Southwest Utah$/                      |                                                                       | /^Utah$/                                                            |
| /^49053$/         | /^Washington County$/;c Washington      |                                                                       |                                                                     |
|                   | c Umatilla County                       |                                                                       | /^Umatilla, OR$/;c OR                                               |
| /^46113$/;c 46102 |                                         |                                                                       |                                                                     |
|                   | /^Wade Hampton Census Area$/;c Kusilvak |                                                                       | /^Alaska$/                                                          |
|                   |                                         |                                                                       | /^Washington, D.C.$/;c District of Columbia                         |
|                   |                                         | /^Cayman Islands$/;c United Kingdom                                   | c Cayman Islands                                                    |
|                   |                                         | /^Channel Islands$/;c United Kingdom                                  | c Channel Islands                                                   |
|                   |                                         | /^Gibraltar$/;c United Kingdom                                        | c Gibraltar                                                         |
|                   |                                         | /^United Kingdom$/                                                    | /^Falkland Islands (Islas Malvinas)$/;c Falkland Islands (Malvinas) |
|                   |                                         | /^Brunei$/;c Brunei Darussalam                                        |                                                                     |
|                   |                                         | /^Congo (Kinshasa)$/;c Congo, Dem. Rep.                               |                                                                     |
|                   |                                         | /^Congo (Brazzaville)$/;c Congo, Rep.                                 |                                                                     |
|                   |                                         | /^Republic of the Congo$/;c Congo, Rep.                               |                                                                     |
|                   |                                         | /^Ivory Coast$/;c Cote d'Ivoire                                       |                                                                     |
|                   |                                         | /^Cape Verde$/;c Cabo Verde                                           |                                                                     |
|                   |                                         | /^The Gambia$/;c Gambia, The                                          |                                                                     |
|                   |                                         | /^Gambia$/;c Gambia, The                                              |                                                                     |
|                   |                                         | /^Egypt$/;c Egypt, Arab Rep.                                          |                                                                     |
|                   |                                         | /^Syria$/;c Syrian Arab Republic                                      |                                                                     |
|                   |                                         | /^Reunion$/;c France                                                  | c Reunion                                                           |
|                   |                                         | /^Diamond Princess$/;c Diamond Princess                               | c                                                                   |
|                   |                                         | /^United States$/                                                     | /^Unassigned Location (From Diamond Princess)$/;c Diamond Princess  |
| /^88888$/;c 88000 |                                         | /^United States$/                                                     | /^Diamond Princess$/                                                |
|                   |                                         | /^Others$/;c Diamond Princess                                         | /^Cruise Ship$/;c                                                   |
|                   |                                         | /^United States$/                                                     | /^Grand Princess Cruise Ship$/;c Grand Princess                     |
| /^99999$/;c 99000 |                                         | /^United States$/                                                     | /^Grand Princess$/                                                  |
|                   |                                         | /^Others$/;c Diamond Princess                                         | /^Diamond Princess cruise ship$/;c                                  |
|                   |                                         | /^Cruise Ship$/;c Diamond Princess                                    | /^Diamond Princess$/;c                                              |
|                   |                                         | /^Faroe Islands$/;c Denmark                                           | c Faroe Islands                                                     |
|                   |                                         | /^Greenland$/;c Denmark                                               | c Greenland                                                         |
|                   |                                         | /^Republic of Ireland$/;c Ireland                                     |                                                                     |
|                   |                                         | /^Saint Barthelemy$/;c France                                         | c Saint Barthélemy                                                  |
|                   |                                         | /^France$/                                                            | /^Saint Barthelemy$/;c Saint Barthélemy                             |
|                   |                                         | /^Saint Lucia$/;c St. Lucia                                           |                                                                     |
|                   |                                         | /^Saint Martin$/;c France                                             | c St Martin                                                         |
|                   |                                         | /^St. Martin$/;c France                                               | c St. Martin (French part)                                          |
|                   |                                         | /^France$/;c France                                                   | /^St Martin$/;c St. Martin (French part)                            |
|                   |                                         | /^Guadeloupe$/;c France                                               | c Guadeloupe                                                        |
|                   |                                         | /^French Guiana$/;c France                                            | c French Guiana                                                     |
|                   |                                         | /^Martinique$/;c France                                               | c Martinique                                                        |
|                   |                                         | /^Mayotte$/;c France                                                  | c Mayotte                                                           |
|                   |                                         | /^Saint Vincent and the Grenadines$/;c St. Vincent and the Grenadines |                                                                     |
|                   |                                         | /^Slovakia$/;c Slovak Republic                                        |                                                                     |
|                   |                                         | /^Venezuela$/;c Venezuela, RB                                         |                                                                     |
|                   |                                         | /^Holy See$/;c Holy See (Vatican City State)                          |                                                                     |
|                   |                                         | /^Vatican City$/;c Holy See (Vatican City State)                      |                                                                     |
|                   |                                         | /^Vatican$/;c Holy See (Vatican City State)                           |                                                                     |
|                   |                                         | /^Ukraine$/                                                           | /Unknown/d                                                          |

originally, it was inside covid.org.  here's code to find the table,
and export it as a .csv file
#+name: buildsedcsv
#+begin_src elisp :var table="sedcsvtable" :results none
  (let ((regexp
         (rx bol (zero-or-more space)
             "#" (zero-or-more space)
             "+" (zero-or-more space)
             "name" (zero-or-more space)
             ":" (zero-or-more space)
             (literal table))
         ))
    (goto-char (point-min))
    (re-search-forward regexp)
    (re-search-forward org-table-line-regexp)
    (org-table-export "./build/sedcsv.csv" "orgtbl-to-csv"))
#+end_src
goto the table
#+begin_src elisp
  (org-link-open-from-string "[[sedcsvtable][x]]")
  (forward-line)
  (org-table-export --output-csv-file-name "orgtbl-to-csv")
#+end_src


#+name: sedcsvtable
#+begin_src sh :results raw
  orgtbl-query -x ~/work/2020/covid19/mine/covid19.org csvsedtable: | \
      grep -v '^|-*+-*|$' | \
      awk -F'|' '
        BEGIN {
          colnames["country"] = "Country_Region"; # or, "Country/Region"
          colnames["province"] = "Province_State"; # or, "Province/State"
          colnames["fips"] = "FIPS"; # but, not always present
          colnames["admin2"] = "Admin2" # but, not always present

          for (i in colnames) {
            printf("| %s ", colnames[i]);
          }
          printf("|\n");
        }
        function quote(x) {
          if (index(x, "\"")) {
            gsub("\"", "\"\"", x);
            return "\"" x "\"";
          } else if (index(x, ",")) {
            return "\"" x "\"";
          } else {
            return x;
          }
        }
        function clr() {
          delete items
          delete seen
        }
        function trim(x) {
          sub("[ /]*$", "", x);
          sub("^[ /]*", "", x);
          return x;
        }
        function bradd(item, x, prefix                   , array, y, z) {
          split(x, array, "=+");
          y = trim(array[1]);
          z = trim(array[2]);
          if (colnames[y] == "") {
            print "error " NR "-- bad field name in " item ": " y;
            quit;
          }
          if (guards[y] != "") {
            print "error " NR "-- more than one " item " for same field? " y;
            quit;
            guards[y] = 1;
          }
          # print "adding items[" item "," y "] = " z;
          items[item,y] = prefix z;
          seen[item,y] = 1;
          # print "seen[" item "," y "] = 1;"
        }
        function column(item, x, prefix                       , array, y, z) {
          z = split(x, array, ";");
          # print "split " z "; from " x;
          for (i in array) {
            # print "for array[" i "]: " array[i];
            y = trim(array[i]);
            bradd(item, y, prefix);
          }
        }
        function output(                      comma, guard, cmd) {
          for (i in colnames) {
            guarded = seen["guard",i];
            # print "[" i "] guarded = " guarded
            guard = items["guard",i];
            cmd = items["cmd",i];
            if (guarded) {
              guard = "/^" guard "$/"
            }
            if ((!guarded) && (cmd == "")) {
              col = "";
            } else if ((guarded) && (cmd != "")) {
              col = guard ";" cmd;
            } else {
              col = guard cmd;
            }
            # col = quote(col);
            printf("| %s ", col);
          }
          printf("|\n");
        }
        NR > 1 {
          # print;
          clr();
          x = trim($2);
          y = trim($3);
          column("guard", x);
          if (y != "") {
            column("cmd", y, "c ");
          }
          output();
        }'
#+end_src

#+RESULTS: sedcsvtable

 # Local Variables:  #
 # eval: (annotate-mode 1) #
 # End:              #
