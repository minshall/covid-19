# this file specifies textual transformations to perform on the input
# .csv files, to deal with random oddities of same

# these are a *very* restricted form of sed(1) commands.  only 's' and
# 'd' are allowed.

# for "s", after the final delimiter, a single 'g' may be appended, to
# change from "regexpr()" to "gregexpr()".  (additionally, obviously,
# a hash mark in column one starts a comment line, and blank lines are
# allowed.

# for "d", the address pattern, at the beginning of the string, can be
# delimited with either slashes ("/"), or with an arbitrary (?)
# backslash-prefixed character, as in "\Yyou//are\Y".  NB: addresses
# are *not* allowed with the "s" command (out of programmer laziness).

# WARNING: the patterns here are fairly fragile.  if one s/Y/X/, and
# another is s/X/Z/, you may have problems, etc.  i use "," and "\""
# to try to anchor REs to one edge or the other, and that mostly seems
# to work.

# eliminate ", Azerbaijan" and its friends
s/^, /,/
# generic typos
s/" +/"/
s/ +"/"/
# eliminate leading zeros in FIPS
s/^0+//
# eliminate asterisks (disputed political boundaries/alleigiance?)
s/[*]+//
/,Unassigned,Wuhan Evacuee,US,3.22.20 23:45,0,0,4,0,0,0,"Unassigned, Wuhan Evacuee, US"/d
/,North Ireland,2020-02-28T05:43:02,1,0,0/d
/^,Guernsey,/d
/^,Jersey,/d
/,,External territories,Australia/d
/,,Jervis Bay Territory,Australia/d
/^,Nashua,New Hampshire,US/d
/^,Palestine,/d
/^,occupied Palestinian territory,/d
/^,Soldotna,Alaska,/d
/^,Sterling,Alaska,/d
/^"Chicago, IL",/d
/^Chicago,/d
/^"Portland, OR",/d
/^"New York City, NY",/d
/^"New York County, NY",/d
/^,Out-of-state,Tennessee,/d
/^US,US,/d
/^From Diamond Princess,Israel,/d
/^From Diamond Princess,Australia,/d

# 86 people in Hawaii, no cases as of 2020-06-20; not in geoheader
# XXX but, i'm unsure about eliminating these.
# so, condition on being from March (ofsome year?
/^15005,Kalawao,Hawaii,US,3/d
/^15005,Kalawao,Hawaii,US,2020-03/d
/^35013,Doña Ana,New Mexico,US,3/d
/^35013,Doña Ana,New Mexico,US,2020-03/d
/^48301,Loving,Texas,US,3/d
/^48301,Loving,Texas,US,2020-03/d
# way late, a spurious line shows up
/^,,,,2020-11-04 05:25:09,,,0,0,0,0,,,$/d
# another!
/^,,,Belgium,2020-11-13 05:25:30,50.8333,4.469936,0,13891,0,-13891,Belgium,0.0,/d