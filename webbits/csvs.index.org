#+options: toc:nil num:nil
#  ^:{}: disable super/subscripting: https://stackoverflow.com/a/698791/1527747
#+options: ^:{}

* covid19 .csv files derived from the JHU data
    
this directory consists of a set of compressed .csv files derived from
the JHU data.  the [[../bin/colean]] program does some basic cleanup of
the raw files from JHU.  some of the rest of the files are derived by
the [[../bin/coggregate]] program, and the rest by the [[../bin/cowiden]]
program.

** a somewhat cleaned up CSV file produced by [[../bin/colean]]
- [[file:./coleaned.csv.gz][coleaned.csv.gz]] :: is basically the JHU data, with a =Date= column
  added (and cleaned up in some minor ways).  also, an =Iso3c= column
  is added with [[https://en.wikipedia.org/wiki/ISO_3166][ISO 3166_1]] "iso3c" codes (e.g., "IRN" or "USA").  see
  [[moreleaned][below]] for more details.

** various levels of aggregation, produced by [[../bin/coggregate]]
    
- [[file:./province.csv.gz][province.csv.gz]] :: aggregates =coleaned.csv.gz= "up" to the
  =Province_State= level.  (this is likely only of use, currently, in
  the US where =Province_State='s are broken into counties (=Admin2=);
  for all other geographic entities, the data in =province.csv.gz=
  will (should!) be the same as that in =coleaned.csv.gz=.)
- [[file:./country.csv.gz][country.csv.gz]] :: data is aggregated to the country level.  i.e.,
  all data relating to provinces/states within a country are
  aggregated together on a daily basis.  similar to the case with
  =province.csv.gz=, for those countries that do *not* break data down
  to a =Province_State= level, the data in =country.csv.gz= will be
  that same as that in =province.csv.gz= and in =coleaned=.
- [[file:./world.csv.gz][world.csv.gz]] :: data is aggregated to the world.  there is one row
  for each day reported in the data, with Country.Region set to
  "World".
- [[file:./entire.csv.gz][entire.csv.gz]] :: all the above data, but in one [[https://gitlab.com/minshall/covid-19/-/issues/6][single file]].

** a set of .csv.gz files produced by [[../bin/cowiden]]

for each of the files produced by [[../bin/coggregate]], there is a
parallel file, with "wide-" prefixed to the filename, that reproduces
the data in the original file, and adds some columns that might be of
use.  please see [[file:../bin/index.html][../bin]] for more information.

      
** country names

      to ensure uniform naming of countries, and slightly to avoid
      "political" issues, i first map some non-conformant
      Country_Regions in the JHU data into ISO 3166-1 country names,
      as used by the
      
      [[https://databank.worldbank.org/home>World Bank]]
        
      (see the file fixups.sed in the
        
      [[https://gitlab.com/minshall/covid-19>code repository).]]

      i then map the updated country names into ISO 3166-1 three
      letter codes, which are then used for the rest of the
      processing, and used when writing the .csv.gz file.  these codes
      are included in the .csv.gz files in the
      column =Iso3c=.

*** errata

      at least two names in the JHU data don't really have any
      equivalent in the World Bank data: "Others" and "Cruise Ships" (which
      are probably meant to represent the same "area", as "Others" appeared
      (with cruise ships as Province.State) from 07.02.2020 until
      10.03.2020, after which "Cruise Ships" appeared.  these names (with
      spaces squashed out) are left in the file, and appear as the
      Country.Region of those records.

** US county-level data

      there seems to be a desire for US county-level data.  one can extract
      something like ("exactly like"?) that from the output file
      =coleaned.csv.gz=.  using dplyr in R:

#+begin_example
> filter(disaggregated, Country_Region=="USA", Admin2!="")
# A tibble: 23,935 x 13
   Date       FIPS  Admin2 Province_State Country_Region Last_Update        
   <date>     <chr> <chr>  <chr>          <chr>          <dttm>             
 1 2020-02-11 6073  San D… California     USA            2020-02-11 01:23:05
 2 2020-02-12 6073  San D… California     USA            2020-02-11 01:23:05
 3 2020-02-13 6073  San D… California     USA            2020-02-13 03:13:08
 4 2020-02-14 6073  San D… California     USA            2020-02-13 03:13:08
 5 2020-02-15 6073  San D… California     USA            2020-02-13 03:13:08
 6 2020-02-16 6073  San D… California     USA            2020-02-13 03:13:08
 7 2020-02-17 6073  San D… California     USA            2020-02-13 03:13:08
 8 2020-02-18 6073  San D… California     USA            2020-02-13 03:13:08
 9 2020-02-19 6073  San D… California     USA            2020-02-13 03:13:08
10 2020-02-20 6073  San D… California     USA            2020-02-13 03:13:08
# … with 23,925 more rows, and 7 more variables: Lat <dbl>, Long_ <dbl>,
#   Confirmed <dbl>, Deaths <dbl>, Recovered <dbl>, Active <dbl>,
#   Combined_Key <chr>
#+end_example
      (one could say
      

: filter(disaggregated, Country_Region=="USA", FIPS!="")

but that would include state-level data, which might or might not be
desired.)

      while i'm not an expert in JHU's timeseries .csv files, it seems
      that using [[https://tidyr.tidyverse.org/index.html][tidyr's]] [[https://tidyr.tidyverse.org/articles/pivot.html][pivot_wider()]] command, we would come up with
      something that might fit the bill for some people who would like
      to have time series:

#+begin_src R
  timeseries <-
    pivot_wider(
      filter(disaggregated, Country_Region=="USA", Admin2!=""),
      c(FIPS,Admin2,Province_State,Country_Region),
      names_from = Date,
      values_from = Confirmed)
#+end_src

** <<moreleaned>> more detail on [[file:./coleaned.csv.gz][coleaned.csv.gz]].

[[../bin/colean]] attempts to create a single file with all historical
data provided by JHU.  here are *some* (maybe the major) actions it
takes as of the date of this writing (19.04.2021).

on a per-.csv file basis
- there's also some textual transformations (dosed and friends -- see
  the file =fixups.sed= in the repo) on the .csv files, to deal with
  anomalies early on.
- some more textual transformations, in a table =csvsedtable= inside
  the =csvsed= header. these mostly normalize names at the
  =Country_Region=, =Province_State=, and =Admin2= levels; plus some
  fiddling with the odd =FIPS=.
- some JHU columns are dropped: =known_excludes <- c("Incidence_Rate",
  "Case-Fatality_Ratio", "Incident_Rate", "Case_Fatality_Ratio")= as i
  think i can derive those from the existing data. (though, in fact, i
  don't.)

then, after creating an initial dataset with all the .csv files'
contents, the following is done over the entire dataset:
- FIPS and Iso3c columns are added
- part (=compute_intervals=) is dealing with early times when, e.g.,
  Australia (i think) was represented for a while by Australia, then
  by some of the regions, then back to the country, etc.
- then (=propagating=, but also in =colean.R=) deals with making sure
  that every entity has an entry for every date after it first appears
  in a .csv file. this process is very slow, doing lots of
  dplyr::group_by(), etc. (at some point i'd like to switch to
  data.table, partly in the hopes it might be faster; and, actually,
  it's my memory of the complexity of the cleaning code that gives me
  pause.)
- there's filtering: remove duplicates, take only the last observation
  (Last_Update) from a given date for a given entity.
