/* eslint-env browser, jquery, es6 */
/* eslint no-undef: "error"*/



'use strict';                   // let's be pedantic

// support for links

// some global (sigh -- i'm lazy) variables
let country = "",     // which country is selected (for province-level)
    province = "",    // which province is selected (for admin2-level)
    scannedBits = 0,  // last scan found these checked
    opClickOrder,     // array showing order in which other pages button groups clicked
    cvdebug = false;

let gmasks,           // map: group name => group mask
    sksamg,           // inverse map: group mask => group name
    promask,          // bit mask of 'province' Granularity
    admmask,          // bit mask of 'admin2' Granularity
    bitmappingdb,     // object: other pages values => bit
    gmaskmappingdb,   // object: other pages values => bit's group mask
    combosdb,         // set of actually observed bit combinations
    ccombosdb,        // map: country names => combosdb for that country
    pcombosdb,        // map: US state names => combosdb for that state
    countriesdb,      // map: country names => iso3c code
    provincesdb;      // array of US state names

let ct_sel, ct_filter; // connections to crosstalk (for URL replicating)

$(document).ready(function() {
    // import variables
    // from group name to bit mask
    gmasks = new Map(JSON.parse(gmasksjson).map(x => [x['series'], x['gmask']]))
    // from bit mask to group name
    sksamg = new Map(JSON.parse(gmasksjson).map(x => [x['gmask'], x['series']]))
    // bit when selecting =province=
    promask = JSON.parse(promaskjson)[0];
    // bit when selecting =admin2=
    admmask = JSON.parse(admmaskjson)[0];
        // to reduce the risk of accidentally writing the originals...
        // (not sure this makes sense here.)
    bitmappingdb = (function() {
        let bitmapping;
        if (typeof bitmapping === 'undefined') {
            bitmapping = JSON.parse(bitmappingdbjson);
            for (let i in bitmapping) {
                bitmapping[i] = bitmapping[i][0]
            }
        }
        return function() { return bitmapping };
    }());
    gmaskmappingdb = (function() {
        let gmaskmapping;
        if (typeof gmaskmapping === 'undefined') {
            gmaskmapping = JSON.parse(gmaskmappingdbjson);
            for (let i in gmaskmapping) {
                gmaskmapping[i] = gmaskmapping[i][0]
            }
        }
        return function() { return gmaskmapping };
    }());
    combosdb = (function() {
        let combos;
        if (typeof combos === 'undefined') {
            combos = JSON.parse(combosjson);
        }
        return function() { return [...combos] };
    }());
    ccombosdb = function() {
        let ccombos = JSON.parse(ccombosjson);
        return new Map(ccombos.map((x) => [x.Country_Region, x.Combos]))
    }
    pcombosdb = function() {
        let pcombos = JSON.parse(pcombosjson);
        // i can't figure out how to easily 'write-protect' other than this
        return new Map(pcombos.map((x) => [x.Province_State, x.Combos]))
    }
    countriesdb =  function() {
        let countries = JSON.parse(ccombosjson);
        return new Map(countries.map((x) => [x.Country_Region, x.Iso3c]));
    }
    provincesdb =  (function() {
        let provinces;
        if (typeof provinces === 'undefined') {
            provinces = JSON.parse(pcombosjson);
            provinces = provinces.map((x) => x.Province_State)
        }
        return function() { return [...provinces] };
    }());
});

// this is for debugging: take a bit map and return the elements
function bitsToName(bits) {
    let bmap = bitmappingdb(),
        b, rval;

    rval = [];
    for (b in bmap) if (bmap[b]&bits) rval[rval.length]=b
    return rval;
}


// create a map of groups to values for this bit combination
function bitsToMap(bits) {
    let bmap = bitmappingdb(),
        rval = new Map();
    
    for (let key in bmap) {
        let binary = bmap[key],
            gmb = gmaskmappingdb()[key],
            sks = sksamg.get(gmb);

        if (gmb === undefined) {
            console.log(`bitsToMap: gmaskmappingdb missing entry for  ${key} (${binary})`);
        } else if (sks === undefined) {
            console.log(`bitsToMap: sksamg missing entry for ${gmb} (from ${key})`);
        }
        if (binary&bits) {
            rval.set(sks, key);
        }
    }

    return rval;
}

function bitsToSet(bits) {
    return new Set(bitsToMap(bits).values());
}

function namesToBits(names) {
    // now, get bits of all checked otherpages
    let bmap=bitmappingdb();

    return names.reduce((x,y) => bmap[y]|x, 0);
}

function updateUrl() {
    // set the URL to have the 'search terms' that would recreate this
    // page.  at this time, we don't recreate the zoom state of the
    // plotly graph, or any column filtering for the datatable.
    // https://computerrock.com/blog/html5-changing-the-browser-url-without-refreshing-page/
    // https://stackoverflow.com/a/3340186/1527747
    // https://stackoverflow.com/a/3354511/1527747

    cvdebug && console.log('updateUrl');
    // https://stackoverflow.com/a/2541083/1527747
    let url = new URL(window.location.href.split('?')[0]), // current URL, minus search
        ckeys;

    for (let which of ['wexg', 'wexs']) {
        let whiches = $(`#${which} input:checked`)
            .map((i,v) =>
                 $(v).prop('value'))
            .get();
        for (let whichesie of whiches) {
            url.searchParams.append(which, whichesie);
        }
    }
    // now, Combined_Key's
    $('#wex-c select')
        .each((i, e) => {
            ckeys = e.selectize.getValue();
            for (let ckey of ckeys) {
                url.searchParams.append("wex-c", ckey);
            }
        })
    // https://developer.mozilla.org/en-US/docs/Web/API/History/pushState
    window.history.replaceState("", document.title, url.href);
}


function admProCombosdb(bit) {
    // which set of combinations should we look in?  if we have
    // selected a country or a province (we *never*, *ever*, choose
    // both), we start with the combinations they have.  else, use the
    // one for the database as a whole
    let cdb;

    // first, get correct country, province

    if (bit&promask) {
        if (country && (countriesdb().get(country) !== undefined)) {
            cdb = ccombosdb().get(country);
        } else {                // no valid country chosen -- return union
            cdb = [...
                   [...ccombosdb().values()]
                   .map((x) => new Set(x))
                   .reduce((x,y) => setUnion(x,y)).values()]
        }
    } else if (bit&admmask) {
        if (province && (provincesdb().indexOf(province) !== -1)) {
            cdb = pcombosdb().get(province);
        } else {                // no valid province chosen -- return union
            cdb = [...
                   [...pcombosdb().values()]
                   .map((x) => new Set(x))
                   .reduce((x,y) => setUnion(x,y)).values()]
        }
    } else {
        // what are we doing here?
        console.log("admProCombosdb() with no admin2, province");
    }
    return cdb;
}

function combosHaveBits(cdb, bits) {
    // return the subset of the combos in CDB that have BITS on
    return cdb.filter(x => (x&bits)===bits)
}

function areCompatible(names) {
    return compatibleCombos(namesToBits(names)).length != 0;
}

function turnOffGroup(bits, bit) {
    // turn off all bits in BITS that are in BIT's group
    return bits & ~gmaskmappingdb()[bitsToName(bit)];
}

function promote(a, m) {
    // M is a member of the array A; put M at the front; return new
    // array
    let b = a.filter(x => x !== m);

    b.unshift(m);
    return b;
}

function setUnion(a, b) {
    // return a union b
    let result = new Set(a);

    for (let i of [...b]) {
        result.add(i);
    }
    return result;
}

function setDiff(a, b) {
    // return a minus b
    let result = new Set(a);

    for (let i of [...b]) {
        result.delete(i);
    }
    return result;
}

function setSymmDiff(a, b) {
    // symmetric set difference
    return setUnion(setDiff(a, b),
                    setDiff(b, a));
}

function setEqual(a, b) {
    // set equality
    return setSymmDiff(a, b).size == 0;
}

function makeItSo(namesParm) {
    // in otherpages, turn on exactly the radio buttons in NAMES,
    // turning off all other radio buttons
    let names = new Set(namesParm),
        checked = new Set(
            // enclose in [...], otherwise get, e.g., 'c', 'o',
            // ... (i.e., letters)
            [$('#otherpages input:checked').prop('value')]
        ),
        uncheck = new Set(checked), // uncheck everything currently checked
        check = new Set(names);     // and check everything in name

    uncheck <- setDiff(uncheck, names); // but, don't uncheck what we want
    check <- setDiff(check, checked); // and, don't check what is already checked

    cvdebug && console.log('makeItSo:', names);
    for (let v of uncheck) {
        cvdebug && console.log(`makeItSo: unchecking ${v}`);
        $('#otherpages input[value=' + v + ']').trigger('click');
    }
    for (let v of check) {
        cvdebug && console.log(`makeItSo: checking ${v}`);
        $('#otherpages input[value=' + v + ']').trigger('click');
    }
}

function getCheckedBits() {
    // now, get bits of all checked otherpages
    let checkedBits = 0,
        bmap=bitmappingdb();


    $('#otherpages input:checked').each(function() {
        let fname = $(this).attr('id');
        checkedBits = checkedBits | bmap[fname];
    })

    return checkedBits;
}

function getCheckedMap() {
    return bitsToMap(getCheckedBits());
}

function getCheckedSet() {
    return new Set(bitsToName(getCheckedBits()));
}

function reduceCombosByBits(cdb, bits) {
    // return a (possibly improper) subset of CDB each of whose
    // members contains BITS
    return cdb.filter(v => (v&bits) === bits);
}

function compatibleCombos(bits) {
    // return the correct ?combosdb(), based on BITS, order of
    // selection, possible admin2, province, etc.
    let cdb = combosdb(),       // beginning (may switch later)
        compatibleBits = 0;

    // process bits in order of opClickOrder
    for (let g of opClickOrder) {
        let gmask = gmasks.get(g),
            bit = bits&gmask;
        if (bit&(promask|admmask)) { // looking at provinces/admin2s?
            // if we take the this-{province,admin2} combos, do we match our
            // previous?  if so, accept this, and switch to that;
            // otherwise, ignore this
            let tdb = reduceCombosByBits(admProCombosdb(bit),
                                         compatibleBits|bit);
            if (tdb.length) {
                compatibleBits = compatibleBits | bit;
                cdb = tdb;
            }
        } else {
            let tdb = reduceCombosByBits(cdb, compatibleBits|bit);
            if (tdb.length) {
                compatibleBits = compatibleBits | bit;
                cdb = tdb;
            }
        }
    }
    return cdb;
}
    

function scanChecked() {
    let have,
        cesitli,
        // these are the values at entry
        bmap=bitmappingdb(),
        gmap=gmaskmappingdb(),
        bits,
        cdb,
        opages;

    if (cvdebug) {
        if ($(this).prop('checked')) {
            console.log(`scanChecked(${$(this).attr('id')} [checked])`)
        } else {
            console.log(`scanChecked(${$(this).attr('id')} [unchecked])`)
        }
    }

    // keep track of order in which user made clicks
    if ($(this).prop('type') == 'radio') {
        opClickOrder = promote(opClickOrder, $(this).prop('name'));
    }

    // get the right combosdb
    cesitli = compatibleCombos(getCheckedBits());

    // choose the 'largest' of possibilities. if each radio group has
    // a checked member, there (should!) be only one possible answer,
    // so we get that.  back in the R code, we organize to make the
    // 'largest' be 'the simplest'.
    makeItSo(bitsToName(cesitli.reduce((x,y) => Math.max(x,y))));
    // get what we finally settled on
    scannedBits = getCheckedBits();
    cesitli = reduceCombosByBits(cesitli, scannedBits);

    // now, reduce checked and/or compatible by 'or'.  this results in
    // ones (1s) whereever there are counts meeting our (current)
    // specification
    have = cesitli.reduce((x,y)=>x|y);

    // then, run through the buttons, enabling and disabling buttons
    // as we have or don't have counts matching those.  we keep
    // everything in =gmask= -- at least those compatible with *other*
    // selected -- so the user is able to make a *different* selection
    // (otherwise they are stuck till they re-load the page)
    opages = $('#otherpages');
    for (let i in bmap) {
        let input = opages.find(`#${i}`),
            label = opages.find(`label[for=${i}]`);
        if (bmap[i]&have) {
            if (label.hasClass('conflict')) {
                cvdebug && console.log(`removing conflict from ${i}`);
                input.removeClass('conflict');
                label.removeClass('conflict');
            }
        } else {
            if (!label.hasClass('conflict')) {
                cvdebug && console.log(`adding conflict to ${i}`);
                input.addClass('conflict');
                label.addClass('conflict');
            }
        }
    }
    // any country, province selected?
    if (scannedBits&promask) {         // want province -- need country
        $('.onecountry').parent().show();
        // the if statement keeps us from putting cursor *after* user
        // has made a selection
        if (!$('.onecountry input').prop('value')) { // if nothing selected, show cursor
            // bootstrap is about to do a focus (after we exit, called
            // from the event propagation code in jquery), so:
            // https://stackoverflow.com/a/7760544/1527747
            window.setTimeout(x => $('.onecountry input').focus(), 0);  // put cursor there
        }
    } else {                    // no, don't want -- disable
        $('.onecountry').parent().hide();
        country = undefined;
    }
    if (scannedBits&admmask) {
        $('.oneprovince').parent().show();
        if (!$('.oneprovince input').prop('value')) { // if nothing selected, show cursor
            // bootstrap is about to do a focus (after we exit, called
            // from the event propagation code in jquery), so:
            // https://stackoverflow.com/a/7760544/1527747
            window.setTimeout(x => $('.oneprovince input').focus(), 0);  // put cursor there
        }
    } else {
        $('.oneprovince').parent().hide();
        province = undefined;
    }

    updateUrl();                // update URL with new selections

    // hex: https://stackoverflow.com/a/57805/1527747
    cvdebug && console.log(`bits ${scannedBits.toString(16)} (${bitsToName(scannedBits)}); have ${have.toString(16)}; ${country}; ${province}`);
}

function goready() {
    let rstr = '',               // if this changes, we found an error
        val = bitsToMap(scannedBits);

    if (!val.get('aggregation')) {
        rstr =
            rstr.concat("<li>You will need to select a \"Geographical Entity\".</li>")
    }
    if (!((val.get('basicper')))) {
        rstr =
            rstr.concat("<li>You will need to select one of the \"Series\".</lip>")
    }
    if (!((val.get('aggregation') != 'province') ||
                 (country && (country != "")))) {
        rstr =
            rstr.concat("<li>As you have selected \"province\", you will need to select a country.</li>")
    }
    if (!((val.get('aggregation') != 'admin2') ||
                 (province && (province != "")))) {
        rstr =
            rstr.concat("<li>As you have selected \"admin2\", you will need to select a province/state.</li>")
    }
    if (rstr==='') {
        return true;
    } else {
        rstr =
            "<p>You need to finish filling in a few fields before clicking to the next page:</p><ul>".concat(rstr).concat("</ul>")
        throw(Error(rstr))
    }
}

function consurl() {
    let cbits, pbits, rval, val;

    val = bitsToMap(scannedBits);

    goready()                   // make sure we're okay
    if (country) {
        cbits = "-".concat(country.toLowerCase());
    } else if (province) {
        cbits = "-".concat('usa')
        pbits = "-".concat(province.toLowerCase())
    } else {
        cbits = ""
        pbits= ""
    }
    let base =
        [val.get('aggregation'),
         cbits,
         val.get('basicper'),
         val.get('delta'),
         val.get('since')]
        .join('-')
        .replace(/-(basic|level|date)/g, '')
    if (province) {
        base =
            base.concat('/')
            .concat(val.get('aggregation'))
            .concat(cbits)
            .concat(pbits)
    }
    rval =
        base
        .replace(/--+/g, '-')
        .replace(/-+\//, '/')
        .replace(/-$/, '')
        .replace(/ /g, '-')
        .concat('.html');
    return where.concat('/')    // relativize
        .concat(rval);
}

function otherpagesgo(event) {
    let url;

    event.preventDefault();
    try {
        url = consurl();
        // https://stackoverflow.com/a/12983893/1527747
        window.location.href = url;
    }
    catch(e) {
        // there may be a conflict with bootstrap *buttons*
        // https://stackoverflow.com/a/15750649/1527747
        // https://learn.jquery.com/using-jquery-core/avoid-conflicts-other-libraries/

        $('#otherpagesgoerrtext').html(String(e))
        $('#otherpagesgoerr')
            .modal( {
                modal: true,
                title: 'need to make more choices',
                autoOpen: true,
                resizable: false
            })
    }
}

$(() => {
    let countries = countriesdb(),
        provinces = provincesdb();

    $('#select-countries').selectize({
        maxItems:1,
        closeAfterSelect: true,
        openOnFocus: false,     // https://stackoverflow.com/a/53015540/1527747
        options:[...countriesdb().keys()].map(a => { return { text:a, value:a }; }),
        onChange: scanChecked
    });
    $('#select-provinces').selectize({
        maxItems:1,
        closeAfterSelect: true,
        openOnFocus: false,     // https://stackoverflow.com/a/53015540/1527747
        options:provincesdb().map(a => { return {text:a, value:a} }),
        onChange: scanChecked
    });
    opClickOrder = [...gmasks.keys()];
});

// get tooltips/popovers/footnotes, whatever we are using, up and
// running.  for footnotes (but, also 'container:'):
// http://hiphoff.com/creating-hover-over-footnotes-with-bootstrap/
function startQtips() {
    $('[data-for=tooltips]').each(function(i,e) {
        let id = $(e).attr('data-id'),
            value = $(e).attr('data-value');

        if (id) {
            let ele,
                elename;

            if (value) {
                // so, #<id>[value=<value>]
                elename = '#' + id + ' ' + '[value=' + '"' + value + '"' + ']';
                ele = $(elename).parent();
            } else {
                elename = '#' + id + ' ' + '.qtip-q';
                ele  = $(elename);
            }

            ele.addClass('qtip');
            // and, squirrel away tooltip text
            ele.attr('data-qtip-text', $(this).html())
        }
    })

    // now, enable tooltips on all .qtip
    $('.qtip').each(function(i,e) {
        let placement = $(this).attr('data-placement');
        
        if (!placement) {
            placement = ['bottom', 'bottom'][i%2]; // default
        }

        $(this).attr('data-toggle', 'tooltip');
        $(this).tooltip({html:true,
                         title: $(this).attr('data-qtip-text'),
                         container: $(this),
                         html: true,
                         trigger: 'manual',
                         delay: {hide:200},
                         placement:placement});
    })

    // now, certain nodes have tooltips *under* them, and these should
    // show when the parent node shows.  this is to allow "tooltips"
    // to work on the ipad.  (the parent node are otherwise in-active,
    // whereas the child nodes are, e.g., checkboxes.)
    $('.qtip-q').each(function() {
        $(this).click(
            function() {
                $(this).closest('.qtip-enclosing')
                    .find('.qtip')
                    .each(function() {
                        $(this).tooltip('show');
                    })
            })
        $(this).closest('.qtip-enclosing').mouseleave(
            function() {
                $(this).closest('.qtip-enclosing')
                    .find('.qtip')
                    .each(function() {
                        $(this).tooltip('hide');
                    })
            })
    })
}

/* for some reason, htmlwidgets give a height of 960px, but DT doesn't
 * respect it, leading to overflow.  i can't figure out how to
 * accommodate this, so it overlays the following element
 * (otherpages).  so,
 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flow_Layout/Flow_Layout_and_Overflow
 * https://developer.mozilla.org/en-US/docs/Web/CSS/overflow-y
 */
$(() => $('#wexdt').css('height', 'auto'));

// when boostrap, DT, etc., loaded decide which granularity, series,
// etc., to select.  (this is accomplished by putting a call to
// scriptsLoaded() in the .html file after all the other scripts have
// been loaded.)
function scriptsLoaded() {
    let url = new URL(window.location),
        oneChecked,
        ckeys;

    // move otherpages to correct tab
    // https://api.jquery.com/after/
    // (if from DOM, works as a *move*, not a "clone")
    $('#nav-table').after($('#nav-opages'))

    // register for crosstalk events so we can track filter/selections
    // in URL.
    // https://rstudio.github.io/crosstalk/authoring.html#modify_the_javascript_binding
    // 'covid19' is what we use in the R code (in elevels()) as the
    // 'group' parameter to crosstalk::SharedData$new():
    ct_sel = new crosstalk.SelectionHandle('covid19');
    ct_sel.on("change", updateUrl);
    ct_filter = new crosstalk.FilterHandle('covid19');
    ct_filter.on("change", updateUrl);

    // when country or province selections change, catch them
    $('.onecountry input')[0].selectize.on('change', v => { country = countriesdb().get(v); updateUrl(); });
    $('.oneprovince input')[0].selectize.on('change', v => { province = v; updateUrl(); });

    cvdebug && console.log(url.href, [...url.searchParams]);
    // now, look for combined_key: 'wex-c'
    ckeys = url.searchParams.getAll('wex-c');
    if (ckeys.length) {
        let seltize = $('#wex-c select')[0].selectize;
        for (let ckey of ckeys) {
            seltize.addItem(ckey, true);
        }
        seltize.trigger('change'); // *now*, trigger change event
    }

    // now, use url.searchParams to find 'wexg', 'wexs'
    for (let which of ['wexg', 'wexs']) {
        cvdebug && console.log(which);
        for (let got of url.searchParams.getAll(which)) {
            cvdebug && console.log(which, got);
            $(`#${which} input[value="${got}"]`).first()
                .prop('checked', true)
                .trigger('change');
        }
    }

    // if nothing selected at granularity ...
    oneChecked = $('#wexg input')
        .map(function() {
            return $(this).prop('checked')
        })
        .get().reduce((x,y) => x||y);
    if (!oneChecked) {
        // in fact, we leave it 'empty'.  this (with current
        // crosstalk, et al., at least) selects *all*, but doesn't
        // cause plotly and datatables to flail through the data base
        // N times -- especially an issue for larger .html files.

        //        $('#wexg input').each(function() {
        //            $(this).prop('checked', true)
        //                .trigger('change');
        //        })
    }
    // if nothing selected at series ...
    oneChecked = $('#wexs input')
        .map(function() {
            return $(this).prop('checked')
        })
        .get().reduce((x,y) => x||y);
    if (!oneChecked) {
        // ... select the *first*.  (don't just do 'click()', because
        // in *that* case, we'd need to know the initial state, as
        // click() just inverts)
        $('#wexs input')
            .first()
            .prop('checked', true)
            .trigger('change');
    }

    $('#otherpages input').change(null, scanChecked);
    // do an initial scan, to gray out  unavailable selections
    scanChecked();
    // handler for Go button
    $('#otherpages').submit(otherpagesgo);
    // now that *this* is done, initialize 'qtips' (our tooltips)
    startQtips();
}

function dtinitialized() {
    // don't show the 'marker key' (tooltip'ish) column.
    // find the table; get datatables API; access the table; find a
    // column using jQuery selector; make visibility false
    // https://api.jquery.com/category/selectors/
    // https://datatables.net/reference/type/table-selector
    // https://datatables.net/reference/api/
    // https://datatables.net/reference/type/column-selector
    $("#wexdt .dataTable")
        .DataTable()
        .table()
        .column("th:contains('Marker_Key')")
        .visible(false);
}
