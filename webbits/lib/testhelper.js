/* eslint-env browser, jquery, es6 */
/* eslint no-undef: "error"*/

// code for the test harness
//
// we want to list all the .html files reachable from otherpages (so
// the build process can detect potential 404s).

// 1) unselect all
// 
// 2) for each aggregation
//   2a) turn off all beneath
//   2b) turn on this agg
//   2c) for each series
//     3a) turn off all beneath
//     3b) turn on this series
//     3) for each deltas
//     3c) for each dayssince
//       4a) turn on this dayssince
//       4a) return consurl() (or, catch an error)

function generateHtmlList() {
    let rval = [],              // return value
        bval = [],              // and, bad values
        iter,
        so = new Map();         // each selection

    // levels of iteration
    
    $('#otherpages input[name="aggregation"]').each(function() {
        // one extra level of iteration, to deal with
        // province/admin2/neither.  this way, we iterate, no
        // matter where we are, and set the countries and
        // provinces input fields to the value appropriate for the
        // level of aggretation (so, normally =undefined=, but,
        // for =province= or =admin2= levels, we iterate once per
        // allowable value
        if ($(this).attr('value') == 'province') {
            iter = [...countriesdb().keys()].map(v=>[v,undefined])
        } else if ($(this).attr('value') == 'admin2') {
            iter = [...provincesdb()].map(v => [undefined,v])
        } else {
            iter = [[undefined, undefined]]
        }
        so.set("aggregation", $(this).attr('value'));
        for (let [c,p] of iter) {
            $( "#select-countries" ).prop('value', c);
            $( "#select-provinces" ).prop('value', p);
            $('#otherpages input[name="basicper"]').each(function() {
                so.set("basicper", $(this).attr('value'));
                $('#otherpages input[name="delta"]').each(function() {
                    so.set("delta", $(this).attr('value'));
                    $('#otherpages input[name="since"]')
                        .each(function() {
                            let names, url;
                            so.set("since", $(this).attr('value'));
                            names = [...so.values()];
                            if (areCompatible(names)) {
                                makeItSo(names); // check the right buttons
                                scanChecked();   // and, do semantic processing
                                try {
                                    url = consurl();
                                    rval.push(url);
                                    cvdebug && console.log(url);
                                } catch(e) {
                                    let s = '',
                                        hyphen = '';
                                    $('#otherpages input:checked')
                                        .each(function() {
                                            s = s.concat(hyphen,
                                                         $(this).prop('value'));
                                            hyphen = '-';
                                        });
                                    if (c !== undefined) {
                                        s = s.concat(` (country=${c}`);
                                    }
                                    if (p !== undefined) {
                                        s = s.concat(` (province=${p}`);
                                    }
                                    bval.push(s);
                                    console.log("got bad consurl(): ", s, e);
                                }
                                return url;
                            }})
                })
            })
            $( "#select-countries" ).prop('value', '');
            $( "#select-provinces" ).prop('value', '');
        }
    })
    return {good: rval, bad: bval};
}

function insertHtmlList() {
    // https://stackoverflow.com/a/2203063/1527747
    let htmlList = generateHtmlList();

    $('<div>', { id: 'html-list' }).appendTo("body");
    $.each(htmlList['good'],
           (i,e) => $('<a>', { class: 'html-list', href: e })
           .appendTo("#html-list"));

    $('<div>', { id: 'bad-list' }).appendTo("body");
    $.each(htmlList['bad'],
           (i,e) => $('<p>', { class: 'bad-list', text: e })
           .appendTo("#bad-list"));
}

// send back the list of reachable URLs via an HTML PUT
// catch(), then(): https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
// https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event
$(document).ready(() => {
    let errors = 0,  p;

    p = fetch('',
              {
                  method:'PUT',
                  body:JSON.stringify(generateHtmlList()),
                  headers: new Headers({'Content-Type': 'application/json'})
              })
        .then(data => {
            console.log('success:', data);
        })
        .catch((error) => {
            console.error('error:', error)
        })
        .finally(() => {
            console.log('finally!');
            if ((!errors) && (cvdebug <2)) {
                close()
            }
        }); // we're now done
})

function updateUrl() {
}
